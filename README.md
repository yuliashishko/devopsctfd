# CTFD project

Добавляем Bitmani repo:

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
```

## Заказ ресурсов

В Yandex Cloud заказать следущие ресурсы:

- Kubernetes Cluster
- S3 Bucket

## MariaDB

```bash
helm install mariadb bitnami/mariadb -f values/mariadb/values.yaml
```

## Redis

```bash
helm install redis bitnami/redis -f values/redis/values.yaml
```

## Nginx Ingress Controller

Устанавливаем Ingress Controller в отдельное пространство имен `ingress-nginx`, [файл конфигурации](values/ingress/values.yaml):

```bash
helm upgrade --install --create-namespace ingress-nginx bitnami/ingress-nginx -n ingress-nginx -f ./ingress/values/values.yaml
```

После выполнения данной команды в Cloud должен появиться объект Ingress Controller

## Cert Manager

Произведем установку менеджера сертификатов, который будет получать их из Lets Encrypt. 
Сначала устанавливаем сам менеджер:

```bash
helm install cert-manager bitnami/cert-manager --namespace cert-manager --create-namespace --set installCRDs=true
```


Для того, чтобы менеджер мог работать с сертификатами, необходимо настроить сущность `ClusterIssuer`, его конфигурация [здесь](values/cert-manager/prod-issuer.yaml)


Выполняем команду:
```bash
kubectl apply -f ./cert-manager/prod-issuer.yaml
```

Последний шаг - настройка Ingress:

```yaml
ingress:
  enabled: true
  className: nginx
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt-prod # the issuer
  hosts:
    - host: ctfd.lazarev-yaroslav.ru
      paths:
        - path: /
          pathType: ImplementationSpecific
  tls:
   - secretName: chart-tls
     hosts:
       - ctfd.lazarev-yaroslav.ru
```

Далее необходимо применить конфигурацию ingress, после чего сертификат будет автоматически выпущен. 

Чтобы получить доступ к приложению в сети Интернет, необходимо зарегистрировать доменное имя и создать запись DNS с IP-адресом LoadBalancer, который отвечает за трафик приложения.

Результат:

<img src="sert.png" height="400">

## PVC

```bash
kubectl apply -f values/pvc/pv-logs.yaml
kubectl apply -f values/pvc/pv-uploads.yaml
```

## CTFD

```bash
helm install ctfd -f ./ctfd/values.yaml
```

## Grafana. Cluster Monitoring

Создадим отдельное пространство имен для сервисов мониторинга:

```bash 
kubectl create namespace kube-monitoring
```
Добавим репозиторий

```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
```

Обновим репозитории helm
```bash
helm update repo
```

Установим Kube Prometheus Stack

```bash
helm install monitoring prometheus-community/kube-prometheus-stack \
 --namespace kube-monitoring
```

После установки Prometheus и Grafana будут доступны. Также появится LoadBalancer instance, IP-адресс которого можно добавить DNS. После этого панель мониторинга Grafana станет доступна из сети Интернет:

![Grafana works](grafana.png)