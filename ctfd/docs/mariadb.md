helm install mariadb oci://registry-1.docker.io/bitnamicharts/mariadb
Pulled: registry-1.docker.io/bitnamicharts/mariadb:12.2.4
Digest: sha256:b843c04fdccb19de5c69db562fcb573b79e4db0baabf39ca5d4a67c7cc073a7b
NAME: mariadb
LAST DEPLOYED: Sun Jun  4 21:35:26 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
CHART NAME: mariadb
CHART VERSION: 12.2.4
APP VERSION: 10.11.3

** Please be patient while the chart is being deployed **

Tip:

  Watch the deployment status using the command: kubectl get pods -w --namespace default -l app.kubernetes.io/instance=mariadb

Services:

  echo Primary: mariadb.default.svc.cluster.local:3306

Administrator credentials:

  Username: root
  Password : $(kubectl get secret --namespace default mariadb -o jsonpath="{.data.mariadb-root-password}" | base64 -d)

To connect to your database:

  1. Run a pod that you can use as a client:

      kubectl run mariadb-client --rm --tty -i --restart='Never' --image  docker.io/bitnami/mariadb:10.11.3-debian-11-r5 --namespace default --command -- bash

  2. To connect to primary service (read/write):

      mysql -h mariadb.default.svc.cluster.local -uroot -p my_database

To upgrade this helm chart:

  1. Obtain the password as described on the 'Administrator credentials' section and set the 'auth.rootPassword' parameter as shown below:

      ROOT_PASSWORD=$(kubectl get secret --namespace default mariadb -o jsonpath="{.data.mariadb-root-password}" | base64 -d)
      helm upgrade --namespace default mariadb oci://registry-1.docker.io/bitnamicharts/mariadb --set auth.rootPassword=$ROOT_PASSWORD