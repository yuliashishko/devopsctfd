helm install nginx-ingress oci://ghcr.io/nginxinc/charts/nginx-ingress
Pulled: ghcr.io/nginxinc/charts/nginx-ingress:0.17.1
Digest: sha256:876eb9dc0022d4517f36909822454b2422c49c32ca72d615ba0b2ac6947e7977
NAME: nginx-ingress
LAST DEPLOYED: Sun Jun  4 21:41:27 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
The NGINX Ingress Controller has been installed.