Pulled: registry-1.docker.io/bitnamicharts/redis-cluster:8.6.2
Digest: sha256:0770c3d90df72a607b3ddff84d493f39cde9095f43203ff5b6c57b19ea6643a5
NAME: redis
LAST DEPLOYED: Sun Jun  4 21:43:17 2023
NAMESPACE: default
STATUS: deployed
helm install redis oci://registry-1.docker.io/bitnamicharts/redis-cluster
REVISION: 1
TEST SUITE: None
NOTES:
CHART NAME: redis-cluster
CHART VERSION: 8.6.2
APP VERSION: 7.0.11** Please be patient while the chart is being deployed **


To get your password run:
    export REDIS_PASSWORD=$(kubectl get secret --namespace "default" redis-redis-cluster -o jsonpath="{.data.redis-password}" | base64 -d)

You have deployed a Redis&reg; Cluster accessible only from within you Kubernetes Cluster.INFO: The Job to create the cluster will be created.To connect to your Redis&reg; cluster:

1. Run a Redis&reg; pod that you can use as a client:
kubectl run --namespace default redis-redis-cluster-client --rm --tty -i --restart='Never' \
 --env REDIS_PASSWORD=$REDIS_PASSWORD \
--image docker.io/bitnami/redis-cluster:7.0.11-debian-11-r12 -- bash

2. Connect using the Redis&reg; CLI:

redis-cli -c -h redis-redis-cluster -a $REDIS_PASSWORD